pt-BR:

O programa "Instalador de Pacotes Deb de Arrastar e Soltar" foi criado por RJP.

Baixe ou descarregue ou transfira o arquivo de texto ".txt" e o arquivo compactado ".zip".

Todos os créditos e direitos estão incluídos nos arquivos, em respeito ao trabalho voluntário de cada pessoa que participou e colaborou para que estes arquivos pudessem ser disponibilizados nesta página eletrônica.

marcelocripe

- - - - -

de:

Das Programm „Drag-and-Drop-Deb-Paket-Installer“ wurde von RJP erstellt.

Laden Sie die Textdatei „.txt“ und die komprimierte Datei „.zip“ herunter.

Alle Quellenangaben und Rechte sind in den Dateien enthalten, in Anerkennung der ehrenamtlichen Arbeit jeder Person, die daran teilgenommen und mitgearbeitet hat, damit diese Dateien auf dieser Website verfügbar gemacht werden konnten.

marcelocripe

- - - - -

en:

The "Drag and Drop Deb Package Installer" program was created by RJP.

Download the ".txt" text file and the ".zip" compressed file.

All credits and rights are included in the files, in respect for the voluntary work of each person who participated and collaborated so that these files could be made available on this website.

marcelocripe

- - - - -

es:

El programa "Instalador de paquetes Deb de arrastrar y soltar" fue creado por RJP.

Descargue el archivo de texto ".txt" y el archivo comprimido ".zip".

Todos los créditos y derechos están incluidos en los archivos, en respeto al trabajo voluntario de cada persona que participó y colaboró ​​para que estos archivos pudieran estar disponibles en este sitio web.

marcelocripe

- - - - -

fi:

"Drag and Drop Deb Package Installer" -ohjelman loi RJP.

Lataa ".txt"-tekstitiedosto ja pakattu ".zip"-tiedosto.

Kaikki hyvitykset ja oikeudet sisältyvät tiedostoihin jokaisen osallistuneen ja yhteistyöhön osallistuneen henkilön vapaaehtoistyön osalta, jotta nämä tiedostot voitaisiin asettaa saataville tällä verkkosivustolla.

marcelocripe

- - - - -

fr :

Le programme "Installer le package Deb par glisser-déposer" a été créé par RJP.

Téléchargez le fichier texte ".txt" et le fichier compressé ".zip".

Tous les crédits et droits sont inclus dans les fichiers, dans le respect du travail bénévole de chaque personne qui a participé et collaboré afin que ces fichiers puissent être mis à disposition sur ce site.

marcelocripe

- - - - -

it:

Il programma "Trascina e rilascia il programma di installazione del pacchetto Deb" è stato creato da RJP.

Scarica il file di testo ".txt" e il file compresso ".zip".

Tutti i crediti ei diritti sono inclusi nei file, nel rispetto del lavoro volontario di ogni persona che ha partecipato e collaborato affinché questi file potessero essere resi disponibili su questo sito web.
